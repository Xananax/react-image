"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**********************************************************
 * Constants used to track status changes in images
 **********************************************************/
exports.EMPTY = 'EMPTY';
exports.LOADING = 'LOADING';
exports.DONE = 'DONE';
exports.ERROR = 'ERROR';
//# sourceMappingURL=constants.js.map